<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!doctype html>
<html>
<head>
<title>Home page</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="google-site-verification" content="FJZzs4d1SwjOoQZprvZ2TYmbrPGW-69XbrCxGhvm6lw" />
<title>Login</title>
<link href="images/favicon.png" rel="shortcut icon">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link type="text/css" href="css/style.css" rel="stylesheet">
<script src="js/modernizr.js" type="text/javascript"></script>
<script src="js/jquery-min.js" type="text/javascript"></script>
<!--[if lte IE 9]>
  <script src="js/ie.js" type="text/javascript"></script>
<![endif]-->
</head>
<body>
    <div class="main">
        <ul id="cbp-bislideshow" class="cbp-bislideshow">
            <li><img src="images/banner1.jpg" alt="image01"/></li>
            <li><img src="images/banner2.jpg" alt="image02"/></li>
            <li><img src="images/banner3.jpg" alt="image03"/></li>
            <li><img src="images/banner4.jpg" alt="image04"/></li>
        </ul>
    </div>
    <div class="topLogo"><img src="images/logo-top.png" alt="Cheil" width="114" height="37"></div>
    <div class="loginPage">
        <div class="loginBlock">
            <h1 class="logo"><img src="images/logo_euro.png" alt="SAMSUNG"></h1>
            <form:form method="POST" commandName="login" action="${pageContext.request.contextPath}/api/oauth2/login.html">
                <label class="msg error">${message}</label>
                <div class="inputRow">
                    <label>Email id</label>
                    <form:input path="userName" cssClass="form-control"/>
                </div>
                <div class="inputRow">
                    <label>Password</label>
                    <form:password path="password" cssClass="form-control"/>
                </div>
                <!--div class="forgotPassword">
                    <a href="#" title="Forgot password?" id="forgotLink">Forgot password?</a>
                </div-->

                <div class="btn">
                    <input type="submit" value="Log in" class="btn">
                </div>
            </form:form>
            <%--form:form id="forgotpassword" method="POST" commandName="login" action="${pageContext.request.contextPath}/api/oauth2/forget-password.html">
              <label class="msg error"></label>
              <div class="inputRow">
                <label>Email id</label>
                <form:input path="userName" id="userNameF" name="userNameF" cssClass="form-control"/>
              </div>
              <div class="forgotPassword">
                <a href="#" title="Sign In" id="signinLink">Sign In</a>
              </div>
              <div class="btn">
                <input type="submit" value="Get New Password" class="btn">
              </div>
            </form:form--%>
        </div>
    </div>
<script src="js/functions.js" type="text/javascript"></script>
<script src="js/general.js" type="text/javascript"></script>
<script>
    $(function() {
        cbpBGSlideshow.init();
    });
</script>
</body>
</html>

