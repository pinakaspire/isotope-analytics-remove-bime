<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="google-site-verification" content="FJZzs4d1SwjOoQZprvZ2TYmbrPGW-69XbrCxGhvm6lw" />
<title>Projects</title>
<link href="images/favicon.png" rel="shortcut icon">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link type="text/css" href="css/style.css" rel="stylesheet">
<!--[if lte IE 9]>
  <script src="js/ie.js" type="text/javascript"></script> 
<![endif]-->
</head>
<body class="countrySelect" id="selectPage">
<div class="topLogo"><img src="images/logo-top.png" alt="Cheil" width="114" height="37"><a href="${pageContext.request.contextPath}/api/oauth2/logout/" class="logOut">Logout</a></div>
<h1>Select a Product</h1>
<%--ul class="servicesList">
	<li class="socialMedia"><a href="#" title="Social Media"><span class="icon"></span>Social Media</a></li>
    <li class="searchMarketing"><a href="#" title="Search Marketing"><span class="icon"></span>Search Marketing</a></li>
    <li class="webAnalytics"><a href="#" title="Web Analytics"><span class="icon"></span>Web Analytics</a></li>
    <li class="brandMentions"><a href="#" title="Brand Mentions"><span class="icon"></span>Brand Mentions</a></li>
    <li class="master"><a href="#" title="Master"><span class="icon"></span>Master</a></li>
</ul--%>
   <ul class="servicesList">
   <c:forEach var="country" items="${allowedCountries}">
          <c:forEach var="dashboard" items="${country.dashboards}">
            <li class="${dashboard.cssClassName}">
                <a title="${dashboard.name}"
                    href="${pageContext.request.contextPath}/api/oauth2/load-selected-country-data/${country.countryName}/${dashboard.id}"
                    target="_blank">
                    <span class="icon"></span>
                    ${dashboard.name}
                 </a>
            </li>
        </c:forEach>
    </c:forEach>
    </ul>
</body>
</html>
