<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!doctype html>
<html>
<head>
<title>Error Page</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Error Page</title>
<link href="images/favicon.png" rel="shortcut icon">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link type="text/css" href="css/style.css" rel="stylesheet">
<script src="js/modernizr.js" type="text/javascript"></script>
<script src="js/jquery-min.js" type="text/javascript"></script>
<style type="text/css">
.errorPage {background: #fff;box-shadow: 0 1px 11px rgba(0, 0, 0, 0.27);border-radius: 2px;width:100%;max-width:415px; position:absolute; left:50%; top:50%; padding:40px 50px; margin:-180px 0 0 -207px; text-align:center}
.backBtn{ background-color:#fff; color:#000000; border:1px solid #000; line-height:40px; padding:0 25px; font-size:17px; display:inline-block;-webkit-border-radius: 2px;-moz-border-radius: 2px;border-radius: 2px; text-decoration:none}
.errorMsg {position:static; color:#ff0000; font-size:16px; line-height:normal; margin-bottom:20px}
</style>
<!--[if lte IE 9]>
  <script src="js/ie.js" type="text/javascript"></script>
<![endif]-->
</head>
<body>
<div class="errorPage">
     <h1 class="logo"><img src="images/logo_euro.png" alt="SAMSUNG"></h1><br>
    <p class="errorMsg">${errorMsg}</p>
    <a href="${pageContext.request.contextPath}/" class="backBtn">Back to Login</a><br/>
</div>
</body>
</html>