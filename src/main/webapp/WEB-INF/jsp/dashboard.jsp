<%@ page import="java.util.*" %>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="google-site-verification" content="FJZzs4d1SwjOoQZprvZ2TYmbrPGW-69XbrCxGhvm6lw" />
<title>Dashboard</title>
<link href="images/favicon.png" rel="shortcut icon">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link type="text/css" href="css/style.css" rel="stylesheet">
<!--[if lte IE 9]>
  <script src="js/ie.js" type="text/javascript"></script>
<![endif]-->
</head>
<body> 
 <!--userAccessToken:${userAccessToken}
userDashboardURL:${userDashboardURL}-->
<div class="projectName">${classname}</div>
<div class="projectText"><a href="/project-selector">IM</a></div>
<iframe id="iframeDash" src="${userDashboardURL}" frameborder="0" style="height:500px;width:100%" height="100%" width="100%"></iframe>
<script src="js/jquery-min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready( function(){
	var h = $(window).height()-10;
	$("#iframeDash").css("height",h);
})
$(window).on('resize', function () {
	var h = $(window).height()-10;
	$("#iframeDash").css("height",h);		
}).trigger('resize');
if ($('.projectName').text() == 'Galaxy S7'){
	$('.projectText').addClass('imBlock')
}
if ($('.projectName').text() == 'Galaxy A'){
	$('.projectText').addClass('imBlock')
}
if ($('.projectName').text() == 'Samsung QLED'){
	$('.projectText').addClass('seBlock')
}


</script>
</body>
</html>