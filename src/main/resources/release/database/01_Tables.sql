create schema analytics_samsung_spain;
use analytics_samsung_spain;

drop table if exists user;
create table user
(
  user_id int not null auto_increment key,
  user_name varchar(100) not null, -- primary key
  user_password varchar(100) not null, -- encrypted (one-way/hash algorithm; MD5/SHA1 not strong enough - SHA256)
  timezone varchar(60), -- store master data in reference table
  dbstatus varchar(10), -- Created, Modified, Dropped
  created datetime,
  updated datetime
);

drop table if exists user_dashboard;
create table user_dashboard
(
  ud_id int not null auto_increment key,
  name varchar(100) not null,
  country_name varchar(60) not null,
  country_code varchar(60),
  user_id integer, -- join to user
  dashboard_url varchar(1000) not null,
  created datetime,
  updated datetime
  );


create unique index user_id on user (user_id);
create index user_name on user (user_name);
create index user_password on user (user_password);

create unique index ud_id on user_dashboard (ud_id);
create index dash_country_name on user_dashboard (country_name);
create index dash_name on user_dashboard (name);
create index dashboard_url on user_dashboard (dashboard_url);

alter table user_dashboard add constraint fk_user_dash_user foreign key (user_id)
	  references user (user_id);

--Oct 6th 2016
alter table user_dashboard add column sort_order tinyint(2);
alter table user_dashboard add column css_class_name varchar(100) not null;


