insert into user(user_id,user_name,user_password,dbstatus, timezone, created,updated )
            values (null, 'alam_shukla@hotmail.com', 'Samsung!234', 'Created', null, NOW(), null);

insert into user(user_id,user_name,user_password,dbstatus, timezone, created,updated )
            values (null, 'novinjaiswal@gmail.com', 'Samsung!234', 'Created', null, NOW(), null);

insert into user_dashboard(ud_id, name, country_name, country_code, dashboard_url, user_id,created,updated )
         select   null, 'RECO-SPAIN','Spain', 'SP', 'https://isotope.bime.io/dashboard/942434717AD1D4B681F7E9501F49178BA61DD0A91AF2B64B3FF566539FF46A70?embed=true',
                1 , NOW(), null from user;


insert into user(user_id,user_name,user_password,dbstatus, timezone, created,updated )
            values (null, 'waqar.riaz@cheil.com', 'Samsung!234', 'Created', null, NOW(), null);

insert into user_dashboard(ud_id, name, country_name, country_code, dashboard_url, user_id,created,updated )
         select   null, 'RECO-SPAIN','Spain', 'SP', 'https://isotope.bime.io/dashboard/942434717AD1D4B681F7E9501F49178BA61DD0A91AF2B64B3FF566539FF46A70?embed=true',
                max(user_id), NOW(), null from user;

-- Test data:end


-- changes of June 10th

update user_dashboard set dashboard_url='https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true'
where country_name='Spain' and country_code='SP';


-- start:changes of June 27th

insert into user(user_id,user_name,user_password,dbstatus, timezone, created,updated )
            values (null, 'waqar.riaz@samsung.com', 'Samsung!234', 'Created', null, NOW(), null);

insert into user_dashboard(ud_id, name, country_name, country_code, dashboard_url, user_id,created,updated )
         select   null, 'RECO-SPAIN','Spain', 'SP', 'https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true',
                max(user_id), NOW(), null from user;

-- end:changes of June 27th


-- changes of Oct 7th
update user_dashboard set sort_order=1;
update user_dashboard set css_class_name=name;

-- for alam
delete from user_dashboard where user_id in (1,4);

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Master', 'Spain', 'SP', '1', 'https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true', '2016-09-02 14:50:58', NULL, '1', 'master');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(
null, 'Social Media', 'Spain', 'SP', '1', 'https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true', '2016-09-02 14:50:58', NULL, '2', 'socialMedia');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Search Marketing', 'Spain', 'SP', '1', 'https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true', '2016-09-02 14:50:58', NULL, '3', 'searchMarketing');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Web Analytics', 'Spain', 'SP', '1', 'https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true', '2016-09-02 14:50:58', NULL, '4', 'webAnalytics');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Brand Mentions', 'Spain', 'SP', '1', 'https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true', '2016-09-02 14:50:58', NULL, '5', 'brandMentions');

update user_dashboard set css_class_name='master' where sort_order=1;

-- for waqar
INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Master', 'Spain', 'SP', '4', 'https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true', '2016-09-02 14:50:58', NULL, '1', 'master');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(
null, 'Social Media', 'Spain', 'SP', '4', 'https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true', '2016-09-02 14:50:58', NULL, '2', 'socialMedia');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Search Marketing', 'Spain', 'SP', '4', 'https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true', '2016-09-02 14:50:58', NULL, '3', 'searchMarketing');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Web Analytics', 'Spain', 'SP', '4', 'https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true', '2016-09-02 14:50:58', NULL, '4', 'webAnalytics');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Brand Mentions', 'Spain', 'SP', '4', 'https://isotope.bime.io/dashboard/56A0E66A8FB1AE657B0C462D13C82C6D8188BE78FE3A0633899D7139B81C5652?embed=true', '2016-09-02 14:50:58', NULL, '5', 'brandMentions');


-- 10 Oct
update user_dashboard set dashboard_url ='https://isotope.bime.io/dashboard/CAEF0A76FEA653473C4665487DDA2349ED3FB9A945111A55EF197413F1BBE876?embed=true';

-- update user_dashboard set dashboard_url ='#', country_code='' where name !='Master';

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Master', 'Spain', 'SP', '3', 'https://isotope.bime.io/dashboard/CAEF0A76FEA653473C4665487DDA2349ED3FB9A945111A55EF197413F1BBE876?embed=true', '2016-09-02 14:50:58', NULL, '1', 'master');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(
null, 'Social Media', 'Spain', 'SP', '3', 'https://isotope.bime.io/dashboard/CAEF0A76FEA653473C4665487DDA2349ED3FB9A945111A55EF197413F1BBE876?embed=true', '2016-09-02 14:50:58', NULL, '2', 'socialMedia');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Search Marketing', 'Spain', 'SP', '3', 'https://isotope.bime.io/dashboard/CAEF0A76FEA653473C4665487DDA2349ED3FB9A945111A55EF197413F1BBE876?embed=true', '2016-09-02 14:50:58', NULL, '3', 'searchMarketing');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Web Analytics', 'Spain', 'SP', '3', 'https://isotope.bime.io/dashboard/CAEF0A76FEA653473C4665487DDA2349ED3FB9A945111A55EF197413F1BBE876?embed=true', '2016-09-02 14:50:58', NULL, '4', 'webAnalytics');

INSERT INTO `analytics_samsung_spain`.`user_dashboard`
(`ud_id`,`name`,`country_name`,`country_code`,`user_id`,`dashboard_url`,`created`,`updated`,`sort_order`,`css_class_name`)
VALUES(null, 'Brand Mentions', 'Spain', 'SP', '3', 'https://isotope.bime.io/dashboard/CAEF0A76FEA653473C4665487DDA2349ED3FB9A945111A55EF197413F1BBE876?embed=true', '2016-09-02 14:50:58', NULL, '5', 'brandMentions');



