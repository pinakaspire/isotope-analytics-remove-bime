;(function($){
	$(document).ready( function(){
		
		if( $("#loadData").length) {
			var h = $(window).height()-$('.logoDashboard').outerHeight()-5;
			var w = $(window).width();
			$("#loadData").css("height",h);
			$("#loadData").css("width",w);
		}
	    if( $(".loginBlock").length) {
		   $('#userName').attr('placeholder', 'Email id');
	       $('#password').attr('placeholder', 'Password');
		   
            $(".loginBlock label").click(function() {
                $(this).fadeOut('fast');
                $(this).next().focus();
                return false;
            });

            $('#userName').focusout(function() {
                if($(this).val() == ''){$(this).prev().fadeIn('fast')}
            });
            $('#password').focusout(function() {
                if($(this).val() == ''){$(this).prev().fadeIn('fast')}
            });

             $('#userName').focus(function() {
                $(this).prev().fadeOut('fast')
            });
            $('#password').focus(function() {
                $(this).prev().fadeOut('fast')
            });
        }
		
		if( $("#forgotLink").length) {
            $("#forgotLink").click(function() {
                $('#login').hide();
				$('#forgotpassword').show();
                return false;
            });
		}
		
		if( $("#signinLink").length) {
            $("#signinLink").click(function() {
				$('#forgotpassword').hide();
				$('#login').show();
                return false;
            });
		}
		
		$("#login").validate({
			rules: {
				userName: {
					required: true
				},
				password: {
					required: true
				}
			},
			messages: {
				userName: {
					required: "Please enter user name"
				},
				password: {
					required: "Please enter your password"
				}
			}
		});
	});
	$(window).load(function() {
		if( $(".loginBlock").length) {
			$('#userName').attr('placeholder', 'Email id');
			$('#password').attr('placeholder', 'Password');
		}
	});
})(jQuery);

