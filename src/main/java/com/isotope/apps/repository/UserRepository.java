package com.isotope.apps.repository;

import com.isotope.apps.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Spring Data JPA repository for the User entity.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findOneByUserName(String username);
    User findByUserName(String username);
    User findByUserNameAndPassword(String username, String password);
}
