package com.isotope.apps.repository;

import com.isotope.apps.domain.UserDashboard;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the UserDashboard entity.
 */
public interface UserDashboardRepository extends JpaRepository<UserDashboard, Long> {
    List<UserDashboard> findByUser_id(Long userId);
    UserDashboard findOneById(Long id);
}
