package com.isotope.apps.domain;

import com.isotope.apps.constant.DatabaseStatusEnum;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.*;

/**
 * Created by novin on 3/14/2016.
 */
@Entity
@Table(name = "user",  catalog = "")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    @Column(name = "user_id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "user_name", nullable = false, length = 100)
    private String userName;

    @Basic
    @Column(name = "user_password", nullable = false, length = 100)
    private String password;
    @Basic
    @Column(name = "timezone", nullable = true, length = 60)
    private String timezone;

    @Basic
    @Column(name = "dbstatus", nullable = true, length = 10)
    //@Enumerated(EnumType.STRING)
    private String dbStatus;

    @Basic
    @Column(name = "created", nullable = true)
    private Date created;
    @Basic
    @Column(name = "updated", nullable = true)
    private Date updated;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("countryName DESC")
    private List<UserDashboard> userDashboards = new ArrayList<>(2);

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public void setDbStatus(String dbStatus) {
        this.dbStatus = dbStatus;
    }

    public List<UserDashboard> getUserDashboards() {
        return userDashboards;
    }

    public void setUserDashboards(List<UserDashboard> userDashboards) {
        this.userDashboards = userDashboards;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        User user = (User) object;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +"}";
    }
}
