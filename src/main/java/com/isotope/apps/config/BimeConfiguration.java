//package com.isotope.apps.config;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.stereotype.Component;
//
///**
// * Created by novin on 5/16/2016.
// */
//@Component
//@ConfigurationProperties(prefix="bime")
//public class BimeConfiguration {
//    private static final Logger LOGGER = LoggerFactory.getLogger(BimeConfiguration.class);
//
//    private String apiKey;
//    private String apiSecret;
//    private String accessToken;
//    private String baseUrl;
//    private String accessTokenEndpoint;
//    private String authorizeUrl;
//    private String callbackUrl;
//    private String userGenericPassword;
//
//    public String getApiKey() {
//        return apiKey;
//    }
//
//    public void setApiKey(String apiKey) {
//        this.apiKey = apiKey;
//    }
//
//    public String getApiSecret() {
//        return apiSecret;
//    }
//
//    public void setApiSecret(String apiSecret) {
//        this.apiSecret = apiSecret;
//    }
//    public String getAccessToken() {
//        return accessToken;
//    }
//
//    public void setAccessToken(String accessToken) {
//        this.accessToken = accessToken;
//    }
//
//    public String getBaseUrl() {
//        return baseUrl;
//    }
//
//    public void setBaseUrl(String baseUrl) {
//        this.baseUrl = baseUrl;
//    }
//
//    public String getAccessTokenEndpoint() {
//        return accessTokenEndpoint;
//    }
//
//    public void setAccessTokenEndpoint(String accessTokenEndpoint) {
//        this.accessTokenEndpoint = accessTokenEndpoint;
//    }
//
//    public String getAuthorizeUrl() {
//        return authorizeUrl;
//    }
//
//    public void setAuthorizeUrl(String authorizeUrl) {
//        this.authorizeUrl = authorizeUrl;
//    }
//
//    public String getCallbackUrl() {
//        return callbackUrl;
//    }
//
//    public void setCallbackUrl(String callbackUrl) {
//        this.callbackUrl = callbackUrl;
//    }
//
//    public String getUserGenericPassword() {
//        return userGenericPassword;
//    }
//
//    public void setUserGenericPassword(String userGenericPassword) {
//        this.userGenericPassword = userGenericPassword;
//    }
//}
//
