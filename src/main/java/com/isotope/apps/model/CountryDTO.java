package com.isotope.apps.model;


import java.util.ArrayList;
import java.util.List;

public class CountryDTO implements Comparable{
	private String countryName;
	private String imageName;
    private List<UserDashboardDTO> dashboards = new ArrayList<>(2);



	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

    public List<UserDashboardDTO> getDashboards() {
        return dashboards;
    }

    @Override
	public boolean equals(Object obj) {
		CountryDTO other=(CountryDTO) obj;
		return this.countryName.equals(other.countryName);
	}

	@Override
	public int hashCode() {
		return countryName.hashCode();
	}

    @Override
    public int compareTo(Object o) {
        CountryDTO dto =  (CountryDTO) o;
        if(this.countryName.equalsIgnoreCase(dto.getCountryName())){
            return 0;
        }
        return -1;
    }
}
