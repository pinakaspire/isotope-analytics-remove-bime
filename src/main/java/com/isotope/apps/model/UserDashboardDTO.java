package com.isotope.apps.model;


public class UserDashboardDTO {
	private Long id;
	private String name;
	private String cssClassName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserDashboardDTO(Long id, String name, String cssClassName) {
		this.id = id;
		this.name = name;
		this.cssClassName = cssClassName;
	}

	public String getCssClassName() {
		return cssClassName;
	}

	public void setCssClassName(String cssClassName) {
		this.cssClassName = cssClassName;
	}
}
