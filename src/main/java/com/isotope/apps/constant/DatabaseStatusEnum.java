
package com.isotope.apps.constant;

/**
 * Enum class for Database Status
 */
public enum DatabaseStatusEnum
{
   CREATED("Created"),
   MODIFIED("Modified"),
   DROPPED("Dropped");

    private final String name;

    private DatabaseStatusEnum(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getValue() {
        return toString();
    }
}
