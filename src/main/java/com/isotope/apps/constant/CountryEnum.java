
package com.isotope.apps.constant;

/**
 * Enum class for Database Status
 */
public enum CountryEnum
{
    UNITED_KINGDOM("United Kingdom", "uk.png"),
    FRANCE("France", "france.png"),
    GERMANY("Germany", "germany.png"),
    ITALY("Italy", "italy.png"),
    SPAIN("Spain", "spain.png");
    private final String name;
    private final String imageName;

    CountryEnum(String name, String imageName) {
        this.name = name;
        this.imageName = imageName;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getValue() {
        return toString();
    }

    public String getImageName() {
        return imageName;
    }


}
