package com.isotope.apps.controller;

import com.isotope.apps.model.LoginDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class NavigationController {
	
	@RequestMapping(value="/")
	public ModelAndView mainPage() {
		ModelAndView model = new ModelAndView("login");
		model.addObject("login", new LoginDTO());
		return model;
	}


	@RequestMapping(value="/index")
	public ModelAndView loginPage(HttpSession session) {
		System.out.println("I m inside controller");
		ModelAndView model = new ModelAndView("login");
		model.addObject("login", session.getAttribute("login")!=null ? session.getAttribute("login"):new LoginDTO());
		model.addObject("message", session.getAttribute("message")!=null ? session.getAttribute("message"): StringUtils.EMPTY);
		return model;
	}


	@RequestMapping(value="/project-selector")
	public ModelAndView countrySelector(HttpSession session) {
		System.out.println("dashboard:nj"+ session.getAttribute("userAccessToken"));
		ModelAndView model = new ModelAndView("countryselector");
		model.addObject("allowedCountries", session.getAttribute("allowedCountries"));
		return model;
	}

	@RequestMapping({"/recouk", "/recofrance", "/recogermany","/recoitaly", "/recospain", "/samsungmembers"})
	public ModelAndView dashboard(HttpSession session) {
		System.out.println("dashboard:nj"+ session.getAttribute("userAccessToken"));
		System.out.println("userDashboardURL:"+ session.getAttribute("userDashboardURL"));

		ModelAndView model = new ModelAndView("dashboard");
        model.addObject("userAccessToken", session.getAttribute("userAccessToken"));
		model.addObject("classname", session.getAttribute("classname"));
		model.addObject("userDashboardURL", session.getAttribute("userDashboardURL"));
		model.addObject("dashboardTitle", session.getAttribute("dashboardTitle"));
		return model;
	}

	@RequestMapping(value="/apperrorpage")
	public ModelAndView errorPage(HttpSession session) {
		System.out.println("apperrorpage:");
		ModelAndView model = new ModelAndView("apperrorpage");
		model.addObject("errorMsg", session.getAttribute("errorMsg"));
		return model;
	}

}
