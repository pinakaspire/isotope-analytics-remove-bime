package com.isotope.apps.controller;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.databind.JsonNode;
import com.isotope.apps.config.JHipsterProperties;
import com.isotope.apps.constant.CountryEnum;
import com.isotope.apps.domain.User;
import com.isotope.apps.domain.UserDashboard;
import com.isotope.apps.model.CountryDTO;
import com.isotope.apps.model.LoginDTO;
import com.isotope.apps.model.UserDashboardDTO;
import com.isotope.apps.service.UserService;
import com.isotope.apps.service.email.Email;
import com.isotope.apps.service.email.exception.compose.AttachmentSizeExceededException;
import com.isotope.apps.service.email.exception.send.SendException;
import com.isotope.apps.service.email.sender.MailerService;
//import com.isotope.apps.util.BimeApi;

@RestController
@RequestMapping("/api/oauth2")
public class BimeOAuthController {

    public static final String CURRENT_USER = "currentUser";
    private final Logger log = LoggerFactory.getLogger(BimeOAuthController.class);

//    @Autowired
//    private BimeConfiguration bimeConfiguration;

    @Autowired
    private UserService userService;

    @Autowired
    private MailerService mailerService;

    @Autowired
    private JHipsterProperties jHipsterProperties;

//    private static final Token EMPTY_TOKEN = null;

//    static OAuthService authServiceNovin = new ServiceBuilder()
//            .provider(BimeApi.class)
//            .apiKey("1680e3dcc3c93483857a37c7c55ac6e285129c215b9e7ea9c1396752394a92a7")
//            .apiSecret("ab283e442d7e5605b034ee1d00fc50738e4acdf40949a03d3e5e7ea2969f2b49")
//            .callback("http://localhost:8282/api/oauth2/callback")
//            .build();
//    static Token sTokenNovin = new Token("10475cfc489e5ed31941cb9f1de1b401d1acf8b475c5e6ca737408896da37385", "");


//    private OAuthService getOAuthService(){
//        OAuthService authService = new ServiceBuilder()
//                .provider(BimeApi.class)
//                .apiKey(bimeConfiguration.getApiKey())
//                .apiSecret(bimeConfiguration.getApiSecret())
//                .callback(bimeConfiguration.getCallbackUrl())
//                .build();
//        return authService;
//
//    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView login(@ModelAttribute LoginDTO loginDTO, final HttpSession session) throws IOException {
        ModelAndView modelAndView = new ModelAndView(new RedirectView("/project-selector"));
        ModelAndView loginErrorModel = validatePassword(loginDTO, session);
        if(loginErrorModel == null){
            String bimeLoginId = null;
            User currentUser = (User) session.getAttribute(CURRENT_USER);
            System.out.println("user from session :" + currentUser);
            Set<CountryDTO> allowedCountriesWithDashboards = getCountryAndDahsboardListByUserAccess(currentUser);
            System.out.println("allowedCountries :" + allowedCountriesWithDashboards.size());
            session.setAttribute("allowedCountries", allowedCountriesWithDashboards);
        }else {
            System.out.println("else:");
            modelAndView = loginErrorModel;
        }
        return modelAndView;
    }

    /**
     * This will called when user selects one dashboard from the list of dashboards of the selected country
     *  It will load the specific dashboard
     * @param countryName
     * @param session
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/load-selected-country-data/{countryName}/{dashboardId}", method = RequestMethod.GET)
    public ModelAndView getSelectedCountryChart(@PathVariable String countryName, @PathVariable Long dashboardId, final HttpSession session) throws IOException {
        String message = StringUtils.EMPTY;
        User currentUser = (User) session.getAttribute(CURRENT_USER);
        if(countryName.equals("SamsungMembers")) {
        	ModelAndView modelAndView = new ModelAndView(new RedirectView("/"+countryName.toLowerCase()));
        	String userDashboardURL= getUserDashboardURLForSelectedDashboardId(dashboardId, currentUser);
        	for(UserDashboard dashboard :currentUser.getUserDashboards()){
				if(dashboardId.longValue() == dashboard.getId().longValue()){
					session.setAttribute("classname", dashboard.getName());
					System.out.println(dashboard.getName());
				}
			}
        	session.setAttribute("dashboardTitle", countryName);
        	session.setAttribute("userDashboardURL", userDashboardURL.toString());
        	modelAndView.addObject("userDashboardURL", userDashboardURL.toString());
            return modelAndView;
        }
        
//        JsonNode namedUsersJson = getNamedUsersFromAnalyticService(currentUser);
//        String userAccessToken = getAccessTokenFromAnalyticServiceResponse(namedUsersJson, currentUser);
        String redirectViewName= getRedirectViewNameByCountry(countryName);
        ModelAndView modelAndView = new ModelAndView(new RedirectView("/"+redirectViewName.toLowerCase()));
//        if(StringUtils.isBlank(userAccessToken)){
//            //no access take user to error page
//            log.error("Unable to find access token for User % with country %", currentUser.getUserName(), countryName);
//            message = "Oops! Unable to authenticate with user:"+ currentUser.getUserName() + " with analytic service. Please contact system administrator.";
//            modelAndView = getErrorPageModel(session,message);
//
//        }else {
            //successfully load dashboard of the selected country
            String userDashboardURL= getUserDashboardURLForSelectedDashboardId(dashboardId, currentUser);
            if(StringUtils.isNotBlank(userDashboardURL)){
                StringBuilder finalURL =new StringBuilder(userDashboardURL) ;
                finalURL.append("&access_token=");
//                finalURL.append(userAccessToken);
                modelAndView.addObject("userDashboardURL", finalURL.toString());
				for(UserDashboard dashboard :currentUser.getUserDashboards()){
					if(dashboardId.longValue() == dashboard.getId().longValue()){
						session.setAttribute("classname", dashboard.getName());
						System.out.println(dashboard.getName());
					}
				}
//                session.setAttribute("userAccessToken", userAccessToken);
                session.setAttribute("userDashboardURL", finalURL.toString().trim());
                session.setAttribute("dashboardTitle", redirectViewName);

            }else {
                log.error("User % with country % has empty dashboard URL ", currentUser.getUserName(), countryName);
                message = "Your account doesn't have access to selected country chart. Please contact system administrator.";
                modelAndView = getErrorPageModel(session,message);
            }
//        }
        return modelAndView;
    }

    private Set<CountryDTO> getCountryAndDahsboardListByUserAccess(User currentUser){
        TreeSet<CountryDTO> allowedCountries = new TreeSet<>();
        if(currentUser.getUserDashboards()!= null && !currentUser.getUserDashboards().isEmpty()){
            for(UserDashboard dashboard:currentUser.getUserDashboards()){
                CountryDTO countryDTO = new CountryDTO();
                countryDTO.setCountryName(dashboard.getCountryName());
                countryDTO.setImageName(getImageNameByCountryName(dashboard.getCountryName()));
                allowedCountries.add(countryDTO);
            }

            for(CountryDTO countryDTO :allowedCountries){
                for(UserDashboard dashboard:currentUser.getUserDashboards()) {
                    if(countryDTO.getCountryName().equalsIgnoreCase(dashboard.getCountryName())){
                        countryDTO.getDashboards().add(new UserDashboardDTO(dashboard.getId(), dashboard.getName(), dashboard.getCssClassName()));
                    }
                }
            }
        }
        return allowedCountries.descendingSet();
    }

    private  String getImageNameByCountryName(String countryName) {
        String imageName = StringUtils.EMPTY;
        if(CountryEnum.UNITED_KINGDOM.toString().equalsIgnoreCase(countryName)) {
            imageName = CountryEnum.UNITED_KINGDOM.getImageName();
        } else if(CountryEnum.FRANCE.toString().equalsIgnoreCase(countryName)) {
            imageName = CountryEnum.FRANCE.getImageName();
        } else if(CountryEnum.GERMANY.toString().equalsIgnoreCase(countryName)) {
            imageName = CountryEnum.GERMANY.getImageName();
        } else if(CountryEnum.ITALY.toString().equalsIgnoreCase(countryName)) {
            imageName = CountryEnum.ITALY.getImageName();
        } else if(CountryEnum.SPAIN.toString().equalsIgnoreCase(countryName)) {
            imageName = CountryEnum.SPAIN.getImageName();
        }
        return imageName;
    }

    private String getUserDashboardURLForSelectedDashboardId(Long countryName, User currentUser){
        for(UserDashboard dashboard:currentUser.getUserDashboards()){
            if(countryName.longValue() == dashboard.getId().longValue()){
                return dashboard.getDashboardUrl();
            }
        }
        return null;
    }

    private ModelAndView getErrorPageModel(final HttpSession session, String message){
        ModelAndView modelAndView = new ModelAndView(new RedirectView("/apperrorpage"));
        session.setAttribute("errorMsg", message);
        return modelAndView;
    }

//    private JsonNode getNamedUsersFromAnalyticService(User currentUser) throws IOException {
//        ObjectMapper mapper = new ObjectMapper();
//        OAuthService service = getOAuthService();
//        OAuthRequest nuRequest = new OAuthRequest(Verb.GET, bimeConfiguration.getBaseUrl()+ "/named_users");
//        service.signRequest(new Token(bimeConfiguration.getAccessToken(), ""), nuRequest);
//        org.scribe.model.Response nuResponse = nuRequest.send();
//        System.out.println("UserName:" + currentUser.getUserName() + " nuResponse.getBody():" + nuResponse.getBody());
//        return mapper.readTree(nuResponse.getBody());
//    }


    private ModelAndView validatePassword(final LoginDTO loginDTO, final HttpSession session){
        String errorMsg = StringUtils.EMPTY;
        ModelAndView modelAndView = new ModelAndView(new RedirectView("/index"));
        System.out.println("user UserName:" + loginDTO.getUserName()+ " Password :" + loginDTO.getPassword());
        if(StringUtils.isBlank(loginDTO.getUserName())) {
            errorMsg ="Username can't be null or empty";
        } else if(StringUtils.isBlank(loginDTO.getPassword())){
            errorMsg ="Password can't be null or empty";
        } else if( StringUtils.isNotBlank(loginDTO.getUserName()) && StringUtils.isNotBlank(loginDTO.getPassword())){
            //&& !bimeConfiguration.getUserGenericPassword().equals(loginDTO.getPassword().trim())
            User user = userService.findByUsernameAndPassword(loginDTO.getUserName(), loginDTO.getPassword());
            System.out.println("user found :" + user);

            if(user != null){
                session.removeAttribute("currentUser");
                session.setAttribute("currentUser", user);
            }else {
                errorMsg ="Invalid username or password";
            }
        }
        if(StringUtils.isNotBlank(errorMsg)){
            modelAndView.addObject("login", loginDTO);
            modelAndView.addObject("message",errorMsg);
            session.setAttribute("message",errorMsg);
            session.setAttribute("login", loginDTO);
            return modelAndView;
        }
        return null;
    }

    private String getAccessTokenFromAnalyticServiceResponse(JsonNode namedUsersJson, User currentUser){
        String userAccessToken = StringUtils.EMPTY;
        JsonNode result = namedUsersJson.path("result");
        if(result!= null && result.size()>0) {
            for (JsonNode node : result) {
                String bimeLoginId = node.path("login").asText();
                System.out.println("bimeLoginId:" + bimeLoginId);
                if (StringUtils.isNotBlank(bimeLoginId) && bimeLoginId.equalsIgnoreCase(currentUser.getUserName())) {
                    userAccessToken = node.path("access_token").asText();
                    System.out.println("UserName Matched, Token found for user:" + bimeLoginId + " AccessToken:nj" + userAccessToken);
                }
            }
        }else {
            log.info("result node is null");
        }
        return  userAccessToken;
    }

    private String getRedirectViewNameByCountry(String countryName){
        String redirectViewName = StringUtils.EMPTY;
       
        if(CountryEnum.UNITED_KINGDOM.toString().equalsIgnoreCase(countryName)) {
            redirectViewName = "UK";
        } else if(CountryEnum.FRANCE.toString().equalsIgnoreCase(countryName)) {
            redirectViewName = CountryEnum.FRANCE.getValue();
        } else if(CountryEnum.GERMANY.toString().equalsIgnoreCase(countryName)) {
            redirectViewName = CountryEnum.GERMANY.getValue();
        } else if(CountryEnum.ITALY.toString().equalsIgnoreCase(countryName)) {
            redirectViewName = CountryEnum.ITALY.getValue();
        } else if(CountryEnum.SPAIN.toString().equalsIgnoreCase(countryName)) {
            redirectViewName = CountryEnum.SPAIN.getValue();
        }
        return "Reco" + redirectViewName.toLowerCase();
    }

    @RequestMapping(value = "/forget-password", method = RequestMethod.POST)
    public ModelAndView forgetPassword(@ModelAttribute LoginDTO loginDTO, final HttpSession session) throws IOException {
        String message = StringUtils.EMPTY;
        ModelAndView model = new ModelAndView(new RedirectView("/index"));

        if(StringUtils.isNotBlank(loginDTO.getUserName())){
            User user = userService.findOneByUsername(loginDTO.getUserName());
            if(user!= null) {
                Email email = new Email();
                email.setFromAddress(jHipsterProperties.getMail().getFrom());
                email.getToAddresses().add(loginDTO.getUserName());
                email.setSubject("Please find authentication information");
                StringBuilder sb = new StringBuilder("Hello Sir, <br> Please find requested information as following<br>username: ");
                sb.append(loginDTO.getUserName());
                sb.append("<br>password:" + user.getPassword());
                sb.append("<br>Thanks<br>Team Isotope.");
                email.setBody(sb.toString());
                try {
                    mailerService.send(email);
                    message = "Your password is successfully sent to your emailId.";
                } catch (SendException |AttachmentSizeExceededException e) {
                    e.printStackTrace();
                    message ="Not able to send the password as email. Please try again later";
                }
            }else {
                message ="User not found in the system with username " + loginDTO.getUserName();
            }
        }else {
            message = "Please enter username as emailId to get password as email.";
        }

        session.setAttribute("message", message);
        session.setAttribute("login", loginDTO);
        return model;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView login(final HttpSession session) throws IOException {
        if(session!= null){
            session.invalidate();
        }
        ModelAndView model = new ModelAndView(new RedirectView("/index"));
        model.addObject("login", new LoginDTO());
        return model;
    }



//    @RequestMapping(value={"/callback"}, method = RequestMethod.GET)
//    public String callback(@RequestParam(value="oauth_token", required=false) String oauthToken,
//                           @RequestParam(value="code", required=false) String oauthVerifier, WebRequest request, Map<String, Object> map) {
//
////        OAuthService service = getOAuthService();
//
//        // getting access token
//        Verifier verifier = new Verifier(oauthVerifier);
////        Token accessToken = service.getAccessToken(EMPTY_TOKEN, verifier);
//
//        // store access token as a session attribute
//        request.setAttribute("oauthAccessToken", accessToken, SCOPE_SESSION);
//
//        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
//        HttpSession session = attr.getRequest().getSession(false); //create a new session
//        session.setAttribute("accessToken",accessToken);
//
//        return "settings";
//    }

    @ExceptionHandler({Exception.class, Error.class})
    public ModelAndView anyError(final HttpSession session, Exception ex) {
        ModelAndView modelAndView = new ModelAndView(new RedirectView("/apperrorpage"));
        session.setAttribute("errorMsg", "Oops! Something went wrong! Please contact system administrator.");
        ex.printStackTrace();
        log.error("Exception:" + ex);
        return modelAndView;
    }

}
