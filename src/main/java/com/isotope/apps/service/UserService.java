package com.isotope.apps.service;

import com.isotope.apps.domain.User;
import com.isotope.apps.domain.UserDashboard;
import com.isotope.apps.repository.UserDashboardRepository;
import com.isotope.apps.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Service class for managing users and dashboard
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

//    @Autowired
//    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDashboardRepository userDashboardRepository;

    @Transactional(readOnly = true)
    public User findByUsernameAndPassword(String userName, String password) {
        return userRepository.findByUserNameAndPassword(userName, password);
    }

    @Transactional(readOnly = true)
    public User findOneByUsername(String login) {
        return userRepository.findOneByUserName(login);
    }

    @Transactional(readOnly = true)
    public List<UserDashboard> findAllUserDashboard(Long userId) {
        List<UserDashboard>  userDashboards= userDashboardRepository.findByUser_id(userId);
        return userDashboards;
    }

    @Transactional(readOnly = true)
    public UserDashboard findDashboardById(Long id) {
        UserDashboard  userDashboards= userDashboardRepository.findOneById(id);
        return userDashboards;
    }
}
