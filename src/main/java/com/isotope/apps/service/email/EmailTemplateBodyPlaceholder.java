package com.isotope.apps.service.email;

/**
 * The enum Email template placeholder.
 */
public enum EmailTemplateBodyPlaceholder {

  /**
   * Email email template body placeholder.
   */
  EMAIL
}
