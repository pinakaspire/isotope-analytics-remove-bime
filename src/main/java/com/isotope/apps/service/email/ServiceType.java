package com.isotope.apps.service.email;


public enum ServiceType {

  DEFAULT, EWS_IN, SMTP, EWS_OUT, EFAX, HYLA_FAX;
}
