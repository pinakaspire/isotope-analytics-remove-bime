package com.isotope.apps.service.email;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The type Email.
 */
public class Email {

  private List<String> toAddresses = new ArrayList<>();
  private List<String> ccAddresses = new ArrayList<>();
  private List<String> bccAddresses = new ArrayList<>();
  private String fromAddress;
  private String subject;
  private String body;
  private EmailPriority emailPriority;
  private List<EmailAttachment> attachments = new ArrayList<>();
  private EmailMessageMimeType messageMimeType;
  private Map<EmailHeader, String> mailHeaders = new HashMap<>();
  private String fromEmailAlias;

  /**
   * Builder email . email builder.
   *
   * @param from    the from
   * @param subject the subject
   * @param body    the body
   * @return the email . email builder
   */
  public static Email.EmailBuilder builder(final String from, String subject, String body) {
    return new Email.EmailBuilder(from, subject, body);
  }

  /**
   * Instantiates a new Email.
   */
  public Email() {
  }

  private Email(List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses,
                String fromAddress, String subject, String body, EmailPriority emailPriority,
                List<EmailAttachment> attachments, EmailMessageMimeType messageMimeType,
                Map<EmailHeader, String> mailHeaders, String fromEmailAlias) {
    this.toAddresses = toAddresses;
    this.ccAddresses = ccAddresses;
    this.bccAddresses = bccAddresses;
    this.fromAddress = fromAddress;
    this.subject = subject;
    this.body = body;
    this.emailPriority = emailPriority;
    this.attachments = attachments;
    this.messageMimeType = messageMimeType;
    this.mailHeaders = mailHeaders;
    this.fromEmailAlias = fromEmailAlias;
  }

  /**
   * Gets body.
   *
   * @return the body
   */
  public String getBody() {
    return body;
  }

  /**
   * Sets body.
   *
   * @param body the body
   */
  public void setBody(final String body) {
    this.body = body;
  }

  /**
   * Gets subject.
   *
   * @return the subject
   */
  public String getSubject() {
    return subject;
  }

  /**
   * Sets subject.
   *
   * @param subject the subject
   */
  public void setSubject(final String subject) {
    this.subject = subject;
  }

  /**
   * Gets email priority.
   *
   * @return the email priority
   */
  public EmailPriority getEmailPriority() {
    return emailPriority;
  }

  /**
   * Sets email priority.
   *
   * @param anEmailPriority the an email priority
   */
  public void setEmailPriority(final EmailPriority anEmailPriority) {
    emailPriority = anEmailPriority;
  }

  /**
   * Gets message mime type.
   *
   * @return the message mime type
   */
  public EmailMessageMimeType getMessageMimeType() {
    return messageMimeType;
  }

  /**
   * Sets message mime type.
   *
   * @param messageMimeType the message mime type
   */
  public void setMessageMimeType(EmailMessageMimeType messageMimeType) {
    this.messageMimeType = messageMimeType;
  }

  /**
   * Gets attachments.
   *
   * @return the attachments
   */
  public List<EmailAttachment> getAttachments() {
    return attachments;
  }

  /**
   * Sets attachments.
   *
   * @param attachments the attachments
   */
  public void setAttachments(List<EmailAttachment> attachments) {
    this.attachments = attachments;
  }

  /**
   * Gets mail headers.
   *
   * @return the mail headers
   */
  public Map<EmailHeader, String> getMailHeaders() {
    return mailHeaders;
  }

  /**
   * Sets mail headers.
   *
   * @param mailHeaders the mail headers
   */
  public void setMailHeaders(Map<EmailHeader, String> mailHeaders) {
    this.mailHeaders = mailHeaders;
  }

  /**
   * Gets from email alias.
   *
   * @return the from email alias
   */
  public String getFromEmailAlias() {
    return fromEmailAlias;
  }

  /**
   * Sets from email alias.
   *
   * @param fromEmailAlias the from email alias
   */
  public void setFromEmailAlias(String fromEmailAlias) {
    this.fromEmailAlias = fromEmailAlias;
  }

  /**
   * Gets to addresses.
   *
   * @return the to addresses
   */
  public List<String> getToAddresses() {
    return toAddresses;
  }

  /**
   * Sets to addresses.
   *
   * @param toAddresses the to addresses
   */
  public void setToAddresses(final List<String> toAddresses) {
    this.toAddresses = toAddresses;
  }

  /**
   * Gets cc addresses.
   *
   * @return the cc addresses
   */
  public List<String> getCcAddresses() {
    return ccAddresses;
  }

  /**
   * Sets cc addresses.
   *
   * @param ccAddresses the cc addresses
   */
  public void setCcAddresses(final List<String> ccAddresses) {
    this.ccAddresses = ccAddresses;
  }

  /**
   * Gets bcc addresses.
   *
   * @return the bcc addresses
   */
  public List<String> getBccAddresses() {
    return bccAddresses;
  }

  /**
   * Sets bcc addresses.
   *
   * @param bccAddresses the bcc addresses
   */
  public void setBccAddresses(final List<String> bccAddresses) {
    this.bccAddresses = bccAddresses;
  }

  /**
   * Gets from address.
   *
   * @return the from address
   */
  public String getFromAddress() {
    return fromAddress;
  }

  /**
   * Sets from address.
   *
   * @param fromAddress the from address
   */
  public void setFromAddress(final String fromAddress) {
    this.fromAddress = fromAddress;
  }

  /**
   * The type Email builder.
   */
  public static class EmailBuilder {
    private List<String> toAddresses = new ArrayList<>();
    private List<String> ccAddresses = new ArrayList<>();
    private List<String> bccAddresses = new ArrayList<>();
    private String fromAddress;
    private String subject;
    private String body;
    private EmailPriority emailPriority;
    private List<EmailAttachment> attachments = new ArrayList<>();
    private EmailMessageMimeType messageMimeType;
    private Map<EmailHeader, String> mailHeaders = new HashMap<>();
    private String fromEmailAlias;

    /**
     * Instantiates a new Email builder.
     *
     * @param from    the from
     * @param subject the subject
     * @param body    the body
     */
    EmailBuilder(final String from, String subject, String body) {
      this.fromAddress = from;
      this.subject = subject;
      this.body = body;
    }

    /**
     * To address email . email builder.
     *
     * @param toAddress the to address
     * @return the email . email builder
     */
    public Email.EmailBuilder toAddress(String toAddress) {
      this.toAddresses.add(toAddress);
      return this;
    }

    /**
     * To addresses email . email builder.
     *
     * @param toAddresses the to addresses
     * @return the email . email builder
     */
    public Email.EmailBuilder toAddresses(List<String> toAddresses) {
      this.toAddresses = toAddresses;
      return this;
    }

    /**
     * Cc address email . email builder.
     *
     * @param ccAddress the cc address
     * @return the email . email builder
     */
    public Email.EmailBuilder ccAddress(String ccAddress) {
      this.ccAddresses.add(ccAddress);
      return this;
    }

    /**
     * Cc addresses email . email builder.
     *
     * @param ccAddresses the cc addresses
     * @return the email . email builder
     */
    public Email.EmailBuilder ccAddresses(List<String> ccAddresses) {
      this.ccAddresses = ccAddresses;
      return this;
    }

    /**
     * Bcc address email . email builder.
     *
     * @param bccAddress the bcc address
     * @return the email . email builder
     */
    public Email.EmailBuilder bccAddress(String bccAddress) {
      this.bccAddresses.add(bccAddress);
      return this;
    }

    /**
     * Bcc addresses email . email builder.
     *
     * @param bccAddresses the bcc addresses
     * @return the email . email builder
     */
    public Email.EmailBuilder bccAddresses(List<String> bccAddresses) {
      this.bccAddresses = bccAddresses;
      return this;
    }

    /**
     * Email priority email . email builder.
     *
     * @param emailPriority the email priority
     * @return the email . email builder
     */
    public Email.EmailBuilder emailPriority(EmailPriority emailPriority) {
      this.emailPriority = emailPriority;
      return this;
    }

    /**
     * Attachments email . email builder.
     *
     * @param attachments the attachments
     * @return the email . email builder
     */
    public Email.EmailBuilder attachments(List<EmailAttachment> attachments) {
      this.attachments = attachments;
      return this;
    }

    /**
     * Attachment email . email builder.
     *
     * @param emailAttachment the email attachment
     * @return the email . email builder
     */
    public Email.EmailBuilder attachment(EmailAttachment emailAttachment) {
      this.attachments.add(emailAttachment);
      return this;
    }

    /**
     * Message mime type email . email builder.
     *
     * @param messageMimeType the message mime type
     * @return the email . email builder
     */
    public Email.EmailBuilder messageMimeType(EmailMessageMimeType messageMimeType) {
      this.messageMimeType = messageMimeType;
      return this;
    }

    /**
     * Mail headers email . email builder.
     *
     * @param mailHeaders the mail headers
     * @return the email . email builder
     */
    public Email.EmailBuilder mailHeaders(Map<EmailHeader, String> mailHeaders) {
      this.mailHeaders = mailHeaders;
      return this;
    }

    /**
     * Mail header email . email builder.
     *
     * @param emailHeader the email header
     * @param value       the value
     * @return the email . email builder
     */
    public Email.EmailBuilder mailHeader(EmailHeader emailHeader, String value) {
      this.mailHeaders.put(emailHeader, value);
      return this;
    }

    /**
     * From email alias email . email builder.
     *
     * @param fromEmailAlias the from email alias
     * @return the email . email builder
     */
    public Email.EmailBuilder fromEmailAlias(String fromEmailAlias) {
      this.fromEmailAlias = fromEmailAlias;
      return this;
    }


    /**
     * Build email.
     *
     * @return the email
     */
    public Email build() {
      return new Email(this.toAddresses, this.ccAddresses, this.bccAddresses, this.fromAddress, this.subject,
              this.body, this.emailPriority, this.attachments, this.messageMimeType, this.mailHeaders,
              this.fromEmailAlias);
    }
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.DEFAULT_STYLE);
  }
}
