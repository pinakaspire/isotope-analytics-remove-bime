package com.isotope.apps.service.email.exception.compose;

public class ComposeException extends Exception {

  public ComposeException(String message) {
    super(message);
  }
}
