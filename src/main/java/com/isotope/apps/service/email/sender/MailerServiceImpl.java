package com.isotope.apps.service.email.sender;

import com.isotope.apps.service.email.Email;
import com.isotope.apps.service.email.ServiceType;
import com.isotope.apps.service.email.exception.compose.AttachmentSizeExceededException;
import com.isotope.apps.service.email.exception.send.SendException;
import com.isotope.apps.service.email.validator.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service sends email based on given using configuration provided through @link(Channel).
 */
@Service(value = "mailerService")
public class MailerServiceImpl implements MailerService {

  @Autowired
  private MailerContext mailerContext;

  @Autowired
  private EmailValidator emailFromAddressValidator;

  @Autowired
  private EmailValidator emailRecipientAddressValidator;

  @Override
  public void send(Email email)
          throws AttachmentSizeExceededException,SendException {
    validateEmail(email);
    Mailer mailer = mailerContext.getMailer(ServiceType.DEFAULT);
    mailer.send(email);
  }

  private void validateEmail(final Email email) throws SendException,
          AttachmentSizeExceededException {
    emailFromAddressValidator.validate(email);
    emailRecipientAddressValidator.validate(email);
  }
}
