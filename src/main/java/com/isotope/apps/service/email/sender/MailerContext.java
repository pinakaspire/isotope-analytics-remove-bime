/*
 *
 *  * *************************************************************************|
 *  *  Copyright (c) 2015. Your Company. All rights reserved.           *|
 *  *  Your Company PROPRIETARY/CONFIDENTIAL                            *|
 *  * *************************************************************************|
 *
 */

package com.isotope.apps.service.email.sender;

import com.isotope.apps.service.email.ServiceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Act as factory to decide mail sender based on service type.
 */
@Component
class MailerContext {

  private static final Logger LOGGER = LoggerFactory.getLogger(MailerContext.class);

  @Autowired
  private Mailer smtpMailer;

  /**
   * Gets mailer.
   *
   * @param serviceType the service type
   * @return the mailer
   */
  public Mailer getMailer(final ServiceType serviceType) {
    switch (serviceType) {
      case SMTP:
      case DEFAULT:
        return smtpMailer;
      default:
        LOGGER.error(String.format("No strategy class found for strategy %s", serviceType));
        return null;
    }
  }
}
