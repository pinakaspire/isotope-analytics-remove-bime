package com.isotope.apps.service.email.template;

import com.isotope.apps.service.email.EmailTemplateBodyPlaceholder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

/**
 * Evaluates templates using velocity engine
 */
@Component
public class VelocityMailTemplatizer implements MailTemplatizer {

  @Autowired
  private VelocityEngine velocityEngine;

  @Override
  public String evaluate(Map<EmailTemplateBodyPlaceholder, ?> templateData, String template) {
    final Context context = createContext(templateData);
    final Writer writer = new StringWriter();
    final StringReader reader = new StringReader(template);
    try {
      velocityEngine.evaluate(context, writer, "", reader);
    } catch (VelocityException velocityException) {
      throw new RuntimeException(velocityException);
    }
    return writer.toString();
  }

  private Context createContext(Map<EmailTemplateBodyPlaceholder, ?> templateData) {
    final Context context = new VelocityContext();
    if (templateData != null && !templateData.isEmpty()) {
      for (Map.Entry<EmailTemplateBodyPlaceholder, ?> keyVal : templateData.entrySet()) {
        final EmailTemplateBodyPlaceholder key = keyVal.getKey();
        final Object value = keyVal.getValue();
        context.put(key.name().toLowerCase(), value);
      }
    }
    context.put("StringUtils", new StringUtils());
    context.put("WordUtils", new WordUtils());
    return context;
  }
}
