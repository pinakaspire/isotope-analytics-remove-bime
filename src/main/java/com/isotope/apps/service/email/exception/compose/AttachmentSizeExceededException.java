package com.isotope.apps.service.email.exception.compose;

/**
 * TO indicate that Attachment is exceeding threshold limit
 */
public class AttachmentSizeExceededException extends ComposeException {

  public AttachmentSizeExceededException(String message) {
    super(message);
  }

}
