package com.isotope.apps.service.email.sender;


import com.isotope.apps.service.email.Email;
import com.isotope.apps.service.email.exception.send.SendException;

/**
 * Interface for message sender
 */
interface Mailer {

  void send(Email anEmail) throws SendException;
}
