package com.isotope.apps.service.email.validator;

import com.isotope.apps.service.email.Email;
import com.isotope.apps.service.email.exception.send.SendException;

/**
 * The interface Email validator.
 */
public interface EmailValidator {

  /**
   * Validate boolean.
   *
   * @param email the email
   * @return the boolean
   * @throws SendException the send exception
   */
  boolean validate(Email email) throws SendException;
}
