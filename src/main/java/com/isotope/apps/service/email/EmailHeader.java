package com.isotope.apps.service.email;

/**
 * Key for preparing mail headers while sending out an email.
 */
public enum EmailHeader {

  /**
   * Smtp return path email header.
   */
  SMTP_RETURN_PATH("return-path"),
  /**
   * Ews return path email header.
   */
  EWS_RETURN_PATH("Return-Path"),
  /**
   * Email id email header.
   */
  EMAIL_ID("emailId");

  private String key;

  EmailHeader(String key) {
    this.key = key;
  }

  /**
   * Gets key.
   *
   * @return the key
   */
  public String getKey() {
    return key;
  }

  public String toString() {
    return getKey();
  }
}
