package com.isotope.apps.service.email.template;

import com.isotope.apps.service.email.EmailTemplateBodyPlaceholder;

import java.util.Map;

/**
 * To evaluate mail templates like subject and body with given dynamic parameters
 */
public interface MailTemplatizer {

  String evaluate(Map<EmailTemplateBodyPlaceholder, ?> aTemplateData, String aTemplate);
}
