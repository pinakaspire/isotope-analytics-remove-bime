package com.isotope.apps.service.email;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.IOException;
import java.io.InputStream;

/**
 * The type Email attachment.
 */
public class EmailAttachment {

  private String name;
  private String attachmentType;
  private String contentType;
  private Long documentSize;
  private String fileName;
  private byte[] content;
  private InputStream contentStream;

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets name.
   *
   * @param name the name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * Gets attachment type.
   *
   * @return the attachment type
   */
  public String getAttachmentType() {
    return attachmentType;
  }

  /**
   * Sets attachment type.
   *
   * @param attachmentType the attachment type
   */
  public void setAttachmentType(final String attachmentType) {
    this.attachmentType = attachmentType;
  }

  /**
   * Gets content type.
   *
   * @return the content type
   */
  public String getContentType() {
    return contentType;
  }

  /**
   * Sets content type.
   *
   * @param contentType the content type
   */
  public void setContentType(final String contentType) {
    this.contentType = contentType;
  }

  /**
   * Gets document size.
   *
   * @return the document size
   */
  public Long getDocumentSize() {
    return documentSize;
  }

  /**
   * Sets document size.
   *
   * @param documentSize the document size
   */
  public void setDocumentSize(final Long documentSize) {
    this.documentSize = documentSize;
  }

  /**
   * Gets file name.
   *
   * @return the file name
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * Sets file name.
   *
   * @param fileName the file name
   */
  public void setFileName(final String fileName) {
    this.fileName = fileName;
  }

//  /**
//   * Get content byte [ ].
//   *
//   * @return the byte [ ]
//   */
//  public synchronized byte[] getContent() {
//    try {
//      if (contentStream != null) {
//        this.content = IOUtils.toByteArray(contentStream);
//        IOUtils.closeQuietly(contentStream);
//        contentStream = null;
//      }
//      return content;
//    } catch (final IOException anIOException) {
//      throw new RuntimeException(
//          String.format("Error while reading temporary attachment content from input stream [%s]", fileName),
//          anIOException);
//    }
//  }

  /**
   * Sets content.
   *
   * @param contentSteam the content steam
   */
  public void setContent(final InputStream contentSteam) {
    this.contentStream = contentSteam;
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.DEFAULT_STYLE);
  }
}
