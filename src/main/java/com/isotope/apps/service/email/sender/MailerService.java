package com.isotope.apps.service.email.sender;

import com.isotope.apps.service.email.Email;
import com.isotope.apps.service.email.exception.compose.AttachmentSizeExceededException;
import com.isotope.apps.service.email.exception.send.SendException;

/**
 * Interface for sending emails
 */
public interface MailerService {
   void send(Email anEmail) throws SendException, AttachmentSizeExceededException;
}
