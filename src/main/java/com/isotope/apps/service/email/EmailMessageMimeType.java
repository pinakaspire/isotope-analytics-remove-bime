package com.isotope.apps.service.email;

/**
 * The enum Email message mime type.
 */
public enum EmailMessageMimeType {

  /**
   * Text html email message mime type.
   */
  TEXT_HTML("text/html"),
  /**
   * Text plain email message mime type.
   */
  TEXT_PLAIN("text/plain"),
  /**
   * Text star email message mime type.
   */
  TEXT_STAR("text/*"),
  /**
   * Multipart star email message mime type.
   */
  MULTIPART_STAR("multipart/*"),
  /**
   * Multipart alternative email message mime type.
   */
  MULTIPART_ALTERNATIVE("multipart/alternative"),
  /**
   * Message rfc 822 email message mime type.
   */
  MESSAGE_RFC822("message/rfc822");

  final private String value;

  EmailMessageMimeType(final String aValue) {
    this.value = aValue;
  }

  /**
   * Gets value.
   *
   * @return the value
   */
  public String getValue() {
    return value;
  }
}
