package com.isotope.apps.service.email.sender;

import com.isotope.apps.config.JHipsterProperties;
import com.isotope.apps.service.email.Email;
import com.isotope.apps.service.email.EmailAttachment;
import com.isotope.apps.service.email.EmailHeader;
import com.isotope.apps.service.email.EmailPriority;
import com.isotope.apps.service.email.exception.send.SendException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Sends email through SMTP channel.
 */
@Component
class SmtpMailer implements Mailer, EnvironmentAware {

  private static final Logger LOGGER = LoggerFactory.getLogger(SmtpMailer.class);

  private RelaxedPropertyResolver mailPropertyResolver;

  @Autowired
  private JHipsterProperties jHipsterProperties;

  @Override
  public void setEnvironment(Environment env) {
    this.mailPropertyResolver = new RelaxedPropertyResolver(env, "mail.");
  }

  @Override
  public void send(final Email anEmail) throws SendException {
    LOGGER.info("Sending email using SMTP");
        final String fromEmailAddress = jHipsterProperties.getMail().getFrom();
    anEmail.setFromAddress(fromEmailAddress);
    LOGGER.info("SMPTP out from address: {}", fromEmailAddress);
    final JavaMailSender javaMailSender = getMailSender();

    try {
      //javaMailSender.send((final MimeMessage aMimeMessage) -> prepareMimeMessage(aMimeMessage, anEmail));
    } catch (MailException mailException) {
      throw new SendException(mailException);
    }
  }

  private void prepareMimeMessage(final MimeMessage aMimeMessage, final Email anEmail)
          throws MessagingException, UnsupportedEncodingException {
    aMimeMessage.setHeader(EmailHeader.SMTP_RETURN_PATH.getKey(), jHipsterProperties.getMail().getFrom());

    for (Map.Entry<EmailHeader, String> entry : anEmail.getMailHeaders().entrySet()) {
      aMimeMessage.setHeader(entry.getKey().toString(), entry.getValue());
    }

    final MimeMessageHelper message = new MimeMessageHelper(aMimeMessage, true, StandardCharsets.UTF_8.name());

    setToRecipients(anEmail.getToAddresses(), message);
    setCcRecipients(anEmail.getCcAddresses(), message);
    setBccRecipients(anEmail.getBccAddresses(), message);

    final EmailPriority emailPriority = anEmail.getEmailPriority();
    if (emailPriority != null) {
      message.setPriority(emailPriority.getPriority());
    }

    final String fromEmailAlias = anEmail.getFromEmailAlias();
    final String fromAddress = anEmail.getFromAddress();
    if (StringUtils.isEmpty(fromEmailAlias)) {
      message.setFrom(fromAddress);
    } else {
      message.setFrom(fromAddress, fromEmailAlias);
      message.setReplyTo(fromEmailAlias);
    }

    final String emailSubject = anEmail.getSubject();
    message.setSubject(emailSubject);

    final String emailBody = anEmail.getBody();
    message.setText(emailBody, Boolean.TRUE);

//    final List<EmailAttachment> emailAttachments = anEmail.getAttachments();
//    for (final EmailAttachment emailAttachment : emailAttachments) {
//      message.addAttachment(emailAttachment.getName(), new ByteArrayResource(emailAttachment.getContent()));
//    }
  }

  /*private String constructUndeliveredEmailPath(Channel aChannel) {
    String returnPath;
    final String defaultUndeliveredEmailPath = mailPropertyResolver.getProperty("undeliveredEmailPath");
    if (aChannel == null) {
      returnPath = defaultUndeliveredEmailPath;
    } else {
      String returnAddress = aChannel.getReturnAddress();
      final String username = aChannel.getUsername();

      if (!StringUtils.isEmpty(returnAddress)) {
        returnPath = returnAddress;
      } else if (!StringUtils.isEmpty(username)) {
        returnPath = username;
      } else {
        returnPath = defaultUndeliveredEmailPath;
      }
    }
    return returnPath;
  }*/

  private void setToRecipients(final List<String> toAddresses, final MimeMessageHelper message) throws MessagingException {
    if (toAddresses!= null && !toAddresses.isEmpty()) {
      LOGGER.info("SMTP out to address: {}", toAddresses);
      final String[] tos = StringUtils.toStringArray(toAddresses);
      final String[] trimmedTos = StringUtils.trimArrayElements(tos);
      message.setTo(trimmedTos);
    }
  }

  private void setCcRecipients(final List<String> ccAddresses, final MimeMessageHelper message) throws MessagingException {
    if (ccAddresses!= null && !ccAddresses.isEmpty()) {
      LOGGER.info("SMTP out cc address: {}", ccAddresses);
      final String[] ccs = StringUtils.toStringArray(ccAddresses);
      final String[] trimmedCcs = StringUtils.trimArrayElements(ccs);
      message.setCc(trimmedCcs);
    }
  }

  private void setBccRecipients(final List<String> bccAddresses, final MimeMessageHelper message) throws MessagingException {
    if (bccAddresses!= null && !bccAddresses.isEmpty()) {
      LOGGER.info("SMTP out bcc address: {}", bccAddresses);
      final String[] bccs = StringUtils.toStringArray(bccAddresses);
      final String[] trimmedBccs = StringUtils.trimArrayElements(bccs);
      message.setBcc(trimmedBccs);
    }
  }

  private JavaMailSender getMailSender() {
    return getDefaultMailSender();
  }

  private JavaMailSender getDefaultMailSender() {
    final JavaMailSenderImpl defaultSender = new JavaMailSenderImpl();
    defaultSender.setHost(mailPropertyResolver.getProperty("smtp.host"));
    defaultSender.setPort(Integer.valueOf(mailPropertyResolver.getProperty("smtp.port")).intValue());
    Properties javaMailProperties = new Properties();
    javaMailProperties.setProperty("mail.smtp.auth", "true");
    javaMailProperties.setProperty("mail.smtp.starttls.enable", "true");
    defaultSender.setJavaMailProperties(javaMailProperties);

    defaultSender.setUsername(jHipsterProperties.getMail().getFrom());
    defaultSender.setPassword(jHipsterProperties.getMail().getPassword());
    return defaultSender;
  }

  /*private JavaMailSender getChannelMailSender(Channel aChannel) {
    final String mailServer = aChannel.getServer();
    final String protocol = aChannel.getProtocol();
    final String userName = aChannel.getUsername();
    final String plainTxtPwd = aChannel.getPassword();

    final JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
    javaMailSenderImpl.setHost(mailServer);
    if (aChannel.getPort() != null) {
      javaMailSenderImpl.setPort(aChannel.getPort());
    }
    javaMailSenderImpl.setProtocol(protocol);
    javaMailSenderImpl.setUsername(userName);
    try {
      final String decryptedPassword = PasswordFactory.decrypt(plainTxtPwd);
      javaMailSenderImpl.setPassword(decryptedPassword);
    } catch (final Exception anException) {
      LOGGER.warn("Ignoring exception {} and using value as is", anException);
      javaMailSenderImpl.setPassword(plainTxtPwd);
    }
    final Properties systemProperties = getMailProperties(mailServer);
    javaMailSenderImpl.setJavaMailProperties(systemProperties);
    return javaMailSenderImpl;
  }*/


    private Properties getMailProperties(final String aMailServerHost) {
    final Properties systemProperties = System.getProperties();
    // If true, attempt to authenticate the user using the AUTH command. Defaults to false.
    systemProperties.put("mail.smtp.auth", Boolean.TRUE.toString());
    // If set to a whitespace separated list of hosts, those hosts are trusted. Otherwise, trust depends on the certificate the server presents.
    systemProperties.put("mail.smtp.ssl.trust", aMailServerHost);
    // If true, enables the use of the STARTTLS command (if supported by the server) to switch the connection to a TLS-protected connection before issuing any login commands.
    // Note that an appropriate trust store must configured so that the client will trust the server's certificate.
    // Defaults to false.
    // Use TLS to encrypt communication with SMTP server
    systemProperties.put("mail.smtp.starttls.enable", Boolean.TRUE.toString());
    systemProperties.put("mail.smtp.ssl.enable", mailPropertyResolver.getProperty("smtp.ssl.enable"));
    final String emailDebugPropertyKey = "mail.debug";
    if (LOGGER.isDebugEnabled() || !StringUtils.isEmpty(System.getProperty(emailDebugPropertyKey))) {
      systemProperties.put(emailDebugPropertyKey, Boolean.TRUE.toString());
    }
    LOGGER.info("Setting mail sender properties {}", systemProperties);
    return systemProperties;
  }
}
