package com.isotope.apps.service.email.exception.send;

public class SendException extends Exception {

  public SendException(String message) {
    super(message);
  }

  public SendException(Throwable cause) {
    super(cause);
  }
}
