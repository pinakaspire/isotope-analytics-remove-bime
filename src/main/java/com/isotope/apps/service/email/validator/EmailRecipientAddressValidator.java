package com.isotope.apps.service.email.validator;

import com.isotope.apps.service.email.Email;
import com.isotope.apps.service.email.exception.send.SendException;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

@Component
public class EmailRecipientAddressValidator implements EmailValidator {

  @Override
  public boolean validate(final Email email) throws SendException {
    if (email != null) {
      if (CollectionUtils.isEmpty(email.getToAddresses())
              && CollectionUtils.isEmpty(email.getCcAddresses())
              && CollectionUtils.isEmpty(email.getBccAddresses())) {
        throw new SendException("No recipient provided");
      }
    }
    return Boolean.TRUE;
  }
}
