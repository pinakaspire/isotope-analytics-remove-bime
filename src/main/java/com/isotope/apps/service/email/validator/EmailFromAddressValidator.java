package com.isotope.apps.service.email.validator;

import com.isotope.apps.service.email.Email;
import com.isotope.apps.service.email.exception.send.SendException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class EmailFromAddressValidator implements EmailValidator {

  @Override
  public boolean validate(final Email email) throws SendException {
    if (email != null) {
      if (StringUtils.isBlank(email.getFromAddress())) {
        throw new SendException("From address is not provided");
      }
    }
    return Boolean.TRUE;
  }
}
