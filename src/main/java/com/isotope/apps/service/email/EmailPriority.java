package com.isotope.apps.service.email;

/**
 * The enum Email priority.
 */
public enum EmailPriority {

  /**
   * Normal email priority.
   */
  NORMAL(5), /**
   * High email priority.
   */
  HIGH(3), /**
   * Exigent email priority.
   */
  EXIGENT(1);

  private final int value;

  EmailPriority(final int aValue) {
    value = aValue;
  }

  /**
   * Gets priority.
   *
   * @return the priority
   */
  public int getPriority() {
    return value;
  }
}
