//package com.isotope.apps.util;
//
//import com.isotope.apps.config.BimeConfiguration;
//import org.scribe.builder.api.DefaultApi20;
//import org.scribe.extractors.AccessTokenExtractor;
//import org.scribe.extractors.JsonTokenExtractor;
//import org.scribe.model.*;
//import org.scribe.oauth.OAuth20ServiceImpl;
//import org.scribe.oauth.OAuthService;
//import org.scribe.utils.OAuthEncoder;
//import org.scribe.utils.Preconditions;
//import org.springframework.beans.factory.annotation.Autowired;
//
///**
// * Created by novin on 5/10/2016.
// */
//public class BimeApi  extends DefaultApi20 {
//    @Autowired
//    private static BimeConfiguration bimeConfiguration;
//
//    @Override
//    public String getAccessTokenEndpoint() {
//        return bimeConfiguration.getAccessTokenEndpoint();
//    }
//    @Override
//    public String getAuthorizationUrl(OAuthConfig config)
//    {
//        Preconditions.checkValidUrl(config.getCallback(), "Must provide a valid url as callback. Bime does not support OOB");
//        return String.format(bimeConfiguration.getAuthorizeUrl(), config.getApiKey(), OAuthEncoder.encode(config.getCallback()));
//    }
//
//    @Override
//    public AccessTokenExtractor getAccessTokenExtractor()
//    {
//        return new JsonTokenExtractor();
//    }
//
//
////    @Override
////    public AccessTokenExtractor getAccessTokenExtractor() {
////        return new AccessTokenExtractor() {
////
////            @Override
////            public Token extract(String response) {
////                Preconditions
////                    .checkEmptyString(response,
////                        "Response body is incorrect. Can't extract a token from an empty string");
////
////                Matcher matcher = Pattern.compile(
////                    "\"access_token\" : \"([^&\"]+)\"").matcher(response);
////                if (matcher.find()) {
////                    String token = OAuthEncoder.decode(matcher.group(1));
////                    return new Token(token, "", response);
////                } else {
////                    throw new OAuthException(
////                        "Response body is incorrect. Can't extract a token from this: '"
////                            + response + "'", null);
////                }
////            }
////        };
////    }
////
////    @Override
////    public String getAuthorizationUrl(OAuthConfig config) {
////        // Append scope if present
////        if (config.hasScope()) {
////            return String.format(SCOPED_AUTHORIZE_URL, config.getApiKey(),
////                OAuthEncoder.encode(config.getCallback()),
////                OAuthEncoder.encode(config.getScope()));
////        } else {
////            return String.format(AUTHORIZE_URL, config.getApiKey(),
////                OAuthEncoder.encode(config.getCallback()));
////        }
////    }
//
//    @Override
//    public Verb getAccessTokenVerb() {
//        return Verb.POST;
//    }
//
//    @Override
//    public OAuthService createService(OAuthConfig config) {
//        return new BimeOAuth2Service(this, config);
//    }
//
//    private class BimeOAuth2Service extends OAuth20ServiceImpl {
//
//        private static final String GRANT_TYPE_AUTHORIZATION_CODE = "authorization_code";
//        private static final String GRANT_TYPE = "grant_type";
//        private DefaultApi20 api;
//        private OAuthConfig config;
//
//        public BimeOAuth2Service(DefaultApi20 api, OAuthConfig config) {
//            super(api, config);
//            this.api = api;
//            this.config = config;
//        }
//
//        @Override
//        public Token getAccessToken(Token requestToken, Verifier verifier) {
//            OAuthRequest request = new OAuthRequest(api.getAccessTokenVerb(),
//                api.getAccessTokenEndpoint());
//            switch (api.getAccessTokenVerb()) {
//                case POST:
//                    request.addBodyParameter(OAuthConstants.CLIENT_ID,
//                        config.getApiKey());
//                    request.addBodyParameter(OAuthConstants.CLIENT_SECRET,
//                        config.getApiSecret());
//                    request.addBodyParameter(OAuthConstants.CODE,
//                        verifier.getValue());
//                    request.addBodyParameter(OAuthConstants.REDIRECT_URI,
//                        config.getCallback());
//                    request.addBodyParameter(GRANT_TYPE,
//                        GRANT_TYPE_AUTHORIZATION_CODE);
//                    break;
//                case GET:
//                default:
//                    request.addQuerystringParameter(OAuthConstants.CLIENT_ID,
//                        config.getApiKey());
//                    request.addQuerystringParameter(OAuthConstants.CLIENT_SECRET,
//                        config.getApiSecret());
//                    request.addQuerystringParameter(OAuthConstants.CODE,
//                        verifier.getValue());
//                    request.addQuerystringParameter(OAuthConstants.REDIRECT_URI,
//                        config.getCallback());
//                    if (config.hasScope())
//                        request.addQuerystringParameter(OAuthConstants.SCOPE,
//                            config.getScope());
//            }
//            Response response = request.send();
//            return api.getAccessTokenExtractor().extract(response.getBody());
//        }
//    }
//
//}
